
GFX = {}

function GFX.drawCube(x, y, z, w, h, d)
    w = w or 1.0
    h = h or w
    d = d or h
    local w2 = w/2
    local h2 = h/2
    local d2 = d/2
    GFX.drawSquarePlane(x-w2, y-h2, z-d2, w, h, 'z-')
    GFX.drawSquarePlane(x+w2, y-h2, z+d2, w, h, 'z+')
    GFX.drawSquarePlane(x-w2, y-h2, z+d2, d, h, 'x-')
    GFX.drawSquarePlane(x+w2, y-h2, z-d2, d, h, 'x+')
    GFX.drawSquarePlane(x-w2, y-h2, z-d2, d, w, 'y-')
    GFX.drawSquarePlane(x-w2, y+h2, z+d2, d, w, 'y+')
end

function GFX.drawSquarePlane(x, y, z, w, h, plane)
    local mmat
    if plane == 'z-' then
        mmat = Mat.translate(x, y, z)
    elseif plane == 'z+' then
        mmat = Mat.rotate(math.pi, 'y') * Mat.translate(x, y, z)
    elseif plane == 'x-' then
        mmat = Mat.rotate(math.pi/2, 'y') * Mat.translate(x, y, z)
    elseif plane == 'x+' then
        mmat = Mat.rotate(-math.pi/2, 'y') * Mat.translate(x, y, z)
    elseif plane == 'y-' then
        mmat = Mat.rotate(math.pi/2, 'x') * Mat.translate(x, y, z)
    elseif plane == 'y+' then
        mmat = Mat.rotate(-math.pi/2, 'x') * Mat.translate(x, y, z)
    else
        return
    end
    shader25d:send('mmat', mmat)
    love.graphics.rectangle('fill', 0, 0, w, h)
end

function GFX.drawSquare(x, y, z, w, h, rx, ry, rz)
    local mmat
    if rx and ry and rz then
        mmat = Mat.rotate(ry, 'y') * Mat.rotate(rx, 'x') * Mat.rotate(rz, 'z') * Mat.translate(x, y, z)
    elseif rx and ry then
        mmat = Mat.rotate(ry, 'y') * Mat.rotate(rx, 'x') * Mat.translate(x, y, z)
    elseif rx and rz then
        mmat = Mat.rotate(rx, 'x') * Mat.rotate(rz, 'z') * Mat.translate(x, y, z)
    elseif ry and rz then
        mmat = Mat.rotate(ry, 'y') * Mat.rotate(rz, 'z') * Mat.translate(x, y, z)
    elseif rx then
        mmat = Mat.rotate(rx, 'x') * Mat.translate(x, y, z)
    elseif ry then
        mmat = Mat.rotate(ry, 'y') * Mat.translate(x, y, z)
    elseif rz then
        mmat = Mat.rotate(rz, 'z') * Mat.translate(x, y, z)
    else
        mmat = Mat.translate(x, y, z)
    end
    shader25d:send('mmat', mmat)
    love.graphics.rectangle('fill', 0, 0, w, h)
end

return GFX
