
local json = require('dkjson')
local Mat = require('mat')
local Room = require('room')
local GFX = require('gfx25d')

-- declare a global variable
-- declare('x', 1)
function declare(name, val)
    rawset(_G, name, val or false)
end

-- this function makes implicit global declarations cause a runtime error
-- for safety reasons
-- and typos
-- it makes life easier
function init_G()
    setmetatable(_G, {
        __newindex = function (_, n)
            error("write to undeclared variable " .. n, 2)
        end,
        __index = function (_, n)
            error("read from undeclared variable " .. n, 2)
        end,
    })
end

-- naive method
function table_tostr(t)
    if not t then 
        return nil 
    end
    local str = ''
    for _, v in ipairs(t) do
        str = str .. v .. '\n'
    end
    return (str ~= '') and str or nil
end

function love.load()
    init_G()

    -- graphical stuff
    declare('shader25d')
    --declare('pmat', Mat.orthographic(0, -10, 16/9, 1))
    declare('pmat', Mat.perspective(math.pi/2, -0.001, -1000, 16/10))
    declare('vmat', Mat.identity())
    declare('mmat', Mat.identity())

    -- load shader
    local f_shader = assert(io.open('assets/shader.json', 'r'))
    local shader_text = f_shader:read('*all')
    f_shader:close()
    local shader_json, pos, err = json.decode(shader_text, 1, nil)
    local shader_vertex, shader_fragment
    if not err then
        shader_vertex = table_tostr(shader_json.vertex)
        shader_fragment = table_tostr(shader_json.fragment)
    else
        print('error, reading shader json failed at', pos)
        os.exit()
    end
    
    -- a 2.5D shader
    -- multiplying MVP in software might be an optimization
    -- or not
    shader25d = love.graphics.newShader(shader_fragment, shader_vertex)

    -- game stuff
    declare('max_recurse', 1)
    declare('portal_goto', {})
    declare('game')
    local f_game = assert(io.open('assets/game.json', 'r'))
    local game_text = f_game:read('*all')
    f_game:close()
    -- local pos, err
    game, pos, err = json.decode(game_text, 1, nil)
    if err then
        print('error, reading game json failed at', pos)
        os.exit()
    end

    for k, v in pairs(game.texpath) do
        game.texture[k] = love.graphics.newImage(v)
        game.texture[k]:setFilter('nearest', 'nearest')
        game.texture[k]:setWrap('repeat', 'repeat')
    end

    for _, sub in ipairs(game.room) do
        if sub.gen and sub.rects then
            for j, rect in ipairs(sub.rects) do
                local i = (#sub.walls)
                local x0, y0, w, h = rect[1], rect[2], rect[3], rect[4]
                local x1, y1 = x0+w, y0-h
                sub.walls[i+1] = {x0, y0, w, 0.0}
                sub.walls[i+2] = {x0, y1, w, 0.0}
                sub.walls[i+3] = {x0, y0, h, 1.0}
                sub.walls[i+4] = {x1, y0, h, 1.0}
            end
        end
        if sub.gen and sub.points then
            local last = nil
            for j, point in ipairs(sub.points) do
                if not last then 
                    last = point
                else
                    local x0, y0, x1, y1 = last[1], last[2], point[1], point[2]
                    --if x0 > x1 then x0, x1 = x1, x0 end
                    --if y0 > y1 then y0, y1 = y1, y0 end
                    local w, h  = x1-x0, y1-y0
                    local len = math.sqrt(w*w+h*h)
                    local d = -math.atan2(h, w)/(math.pi*0.5)
                    local i = (#sub.walls)
                    sub.walls[i+1] = {x0, y0, len, d}

                    last = point
                end
            end
        end
        if sub.gen then
            for j, wall in ipairs(sub.walls) do
                local tex = game.texture[sub.texture.wall]
                local x0, y0, x1, y1 = 0, 0, wall[3], sub.height
                local tx0, ty1, tx1, ty0 = 0.0, 0.0, x1*(16/tex:getWidth()), y1*(16/tex:getHeight())
                local vertices = {{x0, y0, tx0, ty0, 255, 255, 255, 255}, 
                                  {x0, y1, tx0, ty1, 255, 255, 255, 255},
                                  {x1, y0, tx1, ty0, 255, 255, 255, 255},
                                  {x1, y1, tx1, ty1, 255, 255, 255, 255}}
                wall.mesh = love.graphics.newMesh(vertices, 'strip', 'static')
                wall.mesh:setTexture(tex)
            end
            do
                local floor = sub.floor
                local tex = game.texture[sub.texture.wall]
                local x0, y0, x1, y1 = 0.0, 0.0, floor[3]-floor[1], floor[4]-floor[2]
                local tx0, ty0, tx1, ty1 = 0.0, 0.0, (x1-x0)*(16/tex:getWidth()), (y1-y0)*(16/tex:getHeight())
                local vertices = {{x0, y0, tx0, ty0, 255, 255, 255, 255}, 
                                  {x0, y1, tx0, ty1, 255, 255, 255, 255},
                                  {x1, y0, tx1, ty0, 255, 255, 255, 255},
                                  {x1, y1, tx1, ty1, 255, 255, 255, 255}}
                floor.mesh = love.graphics.newMesh(vertices, 'strip', 'static')
                floor.mesh:setTexture(game.texture[sub.texture.floor])
            end
            do
                local ceil = sub.ceil
                local tex = game.texture[sub.texture.wall]
                local x0, y0, x1, y1 = 0.0, 0.0, ceil[3]-ceil[1], ceil[4]-ceil[2]
                local tx0, ty0, tx1, ty1 = 0.0, 0.0, (x1-x0)*(16/tex:getWidth()), (y1-y0)*(16/tex:getHeight())
                local vertices = {{x0, y0, tx0, ty0, 255, 255, 255, 255}, 
                                  {x0, y1, tx0, ty1, 255, 255, 255, 255},
                                  {x1, y0, tx1, ty0, 255, 255, 255, 255},
                                  {x1, y1, tx1, ty1, 255, 255, 255, 255}}
                ceil.mesh = love.graphics.newMesh(vertices, 'strip', 'static')
                ceil.mesh:setTexture(game.texture[sub.texture.ceil])
            end
        end
    end

    game.world = love.physics.newWorld(0.0, 0.0, true)
    game.world:setCallbacks(beginContact, endContact, preSolve, postSolve)
    game.physics = {}
    game.physics.player = {}
    game.physics.player.body = love.physics.newBody(game.world, game.player.x, -game.player.z, 'dynamic')
    game.physics.player.shape = love.physics.newRectangleShape(game.player.w, game.player.h)
    game.physics.player.fixture = love.physics.newFixture(game.physics.player.body, game.physics.player.shape)
    game.physics.player.fixture:setUserData({'player'})
    game.physics.player.fixture:setCategory(1)

    game.physics.keys = {}
    for i, v in ipairs(game.keys) do
        game.physics.keys[i] = {}
        game.physics.keys[i].body = love.physics.newBody(game.world, v.x, -v.z, 'dynamic')
        if game.player.sub == v.sub then
            game.physics.keys[i].body:setActive(true)
        else
            game.physics.keys[i].body:setActive(false)
        end
        game.physics.keys[i].shape = love.physics.newRectangleShape(v.w, v.w)
        game.physics.keys[i].fixture = love.physics.newFixture(game.physics.keys[i].body, game.physics.keys[i].shape)
        game.physics.keys[i].fixture:setUserData({'key', v.name, i})
        game.physics.keys[i].fixture:setCategory(2)
    end

    game.physics.locks = {}
    for i, v in ipairs(game.locks) do
        game.physics.locks[i] = {}
        game.physics.locks[i].body = love.physics.newBody(game.world, v.x, -v.z, 'static')
        game.physics.locks[i].body:setActive(true)
        game.physics.locks[i].shape = love.physics.newRectangleShape(v.w, v.w)
        game.physics.locks[i].fixture = love.physics.newFixture(game.physics.locks[i].body, game.physics.locks[i].shape)
        game.physics.locks[i].fixture:setUserData({'lock', v.name, i})
        game.physics.locks[i].fixture:setCategory(3)
    end

    game.physics.subs = {}
    for i, sub in ipairs(game.room) do
        if sub.gen then
            local phsub = {}
            phsub.walls = {}
            for j, wall in ipairs(sub.walls) do
                phsub.walls[j] = {}
                phsub.walls[j].body = love.physics.newBody(game.world)
                if (type(game.player.sub) == 'number' and i == game.player.sub)
                or (type(game.player.sub) == 'string' and sub.name == game.player.sub) then 
                    phsub.walls[j].body:setActive(true)
                else
                    phsub.walls[j].body:setActive(false)
                end
                local a, b, c, d = wall[1], wall[2]
                c = wall[1] + wall[3]*math.cos(wall[4]*math.pi*0.5)
                d = wall[2] + wall[3]*-math.sin(wall[4]*math.pi*0.5)
                phsub.walls[j].shape = love.physics.newEdgeShape(a, -b, c, -d)
                phsub.walls[j].fixture = love.physics.newFixture(phsub.walls[j].body, phsub.walls[j].shape)
                phsub.walls[j].fixture:setCategory(4)
            end

            phsub.portals = {}
            for j, portal in ipairs(sub.portals) do
                phsub.portals[j] = {}
                phsub.portals[j].body = love.physics.newBody(game.world)
                if (type(game.player.sub) == 'number' and i == game.player.sub)
                or (type(game.player.sub) == 'string' and sub.name == game.player.sub) then 
                    phsub.portals[j].body:setActive(true)
                else
                    phsub.portals[j].body:setActive(false)
                end
                local a, b, c, d = portal.shape[1], portal.shape[2]
                c = portal.shape[1] + portal.shape[3]*math.cos(portal.shape[5]*math.pi*0.5)
                d = portal.shape[2] + portal.shape[3]*-math.sin(portal.shape[5]*math.pi*0.5)
                phsub.portals[j].shape = love.physics.newEdgeShape(a, -b, c, -d)
                phsub.portals[j].fixture = love.physics.newFixture(phsub.portals[j].body, phsub.portals[j].shape)
                phsub.portals[j].fixture:setUserData({'portal', j})
                phsub.portals[j].fixture:setCategory(5)
            end

            game.physics.subs[i] = phsub
        else
            game.physics.subs[i] = {}
        end
    end
end

function beginContact(a, b, coll)
    local a_data = a:getUserData()
    local b_data = b:getUserData()
    local portal = 0
    if a_data and a_data[1] == 'player' 
        and b_data and b_data[1] == 'portal' then
        portal = b_data[2]
    elseif a_data and a_data[1] == 'portal' 
        and b_data and b_data[1] == 'player' then
        portal = a_data[2]
    end
    if portal > 0 then
        local subroom = Room.getSub(game.room, game.player.sub)
        local dst = subroom.portals[portal]
        local active = true
        if dst.needs then
            for _, n in ipairs(dst.needs) do
                if not game.locks[n].active then
                    active = false
                end
            end
        end
        if active then
            portal_goto = {}
            portal_goto.out = {}
            portal_goto.out[1] = dst.out[1]
            portal_goto.out[2] = dst.out[2]
            portal_goto.out[3] = dst.out[3]
            portal_goto.out[4] = dst.out[4]
            portal_goto.sub =    dst.sub
        end
    end

    if a_data and a_data[1] == 'key'
        and b_data and b_data[1] == 'lock' then
        if a_data[2] == b_data[2] then
            a:setMask(3)
            game.locks[b_data[3]].active = true
        end
    elseif a_data and a_data[1] == 'lock'
        and b_data and b_data[1] == 'key' then
        if a_data[2] == b_data[2] then
            b:setMask(3)
            game.locks[a_data[3]].active = true
        end
    end
end

function endContact(a, b, coll)
end

function preSolve(a, b, coll)
end

function postSolve(a, b, coll)
end

function love.update(dt)
    game.world:update(dt)
    if portal_goto.out then
        local pos = portal_goto.out
        local sub = portal_goto.sub
        pos[1] = game.player.x + pos[1]
        pos[2] = game.player.z + pos[2]
        game.player.sub = sub
        game.physics.player.body:setPosition(pos[1], -pos[2])
        game.player.r = game.player.r + (pos[4] or pos[3] or 0.0)*math.pi*0.5
        if game.player.grabbed > 0 then
            local key = game.keys[game.player.grabbed]
            key.sub = sub
            local phkey = game.physics.keys[game.player.grabbed]
            local dx = key.x-game.player.x
            local dz = key.z-game.player.z
            local d = math.sqrt(dx*dx+dz*dz)
            local c, s = math.cos(game.player.r), -math.sin(game.player.r)
            phkey.body:setPosition(pos[1]-s*d, -pos[2]+c*d)
        end
        portal_goto = {}
        for i, sub in ipairs(game.physics.subs) do
            if (type(game.player.sub) == 'number' and i == game.player.sub)
            or (type(game.player.sub) == 'string' and Room.getSub(game.room, i).name == game.player.sub) then 
                if sub.walls then   for _, wall in ipairs(sub.walls) do wall.body:setActive(true) end end
                if sub.portals then for _, portal in ipairs(sub.portals) do portal.body:setActive(true) end end
            else
                if sub.walls then   for _, wall in ipairs(sub.walls) do wall.body:setActive(false) end end
                if sub.portals then for _, portal in ipairs(sub.portals) do portal.body:setActive(false) end end
            end
        end
        for i, key in ipairs(game.physics.keys) do
            if game.keys[i].sub == game.player.sub then 
                key.body:setActive(true)
            else
                key.body:setActive(false)
            end
        end
    end
    game.player.x = game.physics.player.body:getX()
    game.player.z = -game.physics.player.body:getY()
    for i, v in ipairs(game.keys) do
        v.x = game.physics.keys[i].body:getX()
        v.z = -game.physics.keys[i].body:getY()
    end

    -- input
    local rsp = 3
    local sp = 3
    local z = math.cos(game.player.r)*sp
    local x = -math.sin(game.player.r)*sp
    local vx = 0
    local vz = 0
    if love.keyboard.isDown('w') then
        vz = -z
        vx = -x
    elseif love.keyboard.isDown('s') then
        vz = z
        vx = x
    end
    if love.keyboard.isDown('a') then
        vz = vz+x
        vx = vx-z
    elseif love.keyboard.isDown('d') then
        vz = vz-x
        vx = vx+z
    end
    game.physics.player.body:setLinearVelocity(vx, -vz)
    if game.player.grabbed > 0 then
        game.physics.keys[game.player.grabbed].body:setLinearVelocity(vx, -vz)
        game.keys[game.player.grabbed].y = 0.7-0.5*math.sin(game.player.rx)
    end

    if love.keyboard.isDown('escape') then
        love.mouse.setRelativeMode(false)
    end
    if love.mouse.isDown(1) then
    end
    vmat = Mat.translate(-game.player.x, -game.player.y, -game.player.z) * Mat.rotate(game.player.r, 'y') * Mat.rotate(game.player.rx, 'x')
end

function love.mousemoved(x, y, dx, dy)
    if love.mouse.getRelativeMode() then
        local rsp = 0.0015
        game.player.r = game.player.r + rsp*dx
        game.player.rx = game.player.rx + rsp*dy

        if game.player.grabbed > 0 then
            local key = game.keys[game.player.grabbed]
            local phkey = game.physics.keys[game.player.grabbed]
            local dx = key.x-game.player.x
            local dz = key.z-game.player.z
            local d = math.sqrt(dx*dx+dz*dz)
            local c, s = math.cos(game.player.r), -math.sin(game.player.r)
            phkey.body:setPosition(game.player.x-s*d, -game.player.z+c*d)
        end
    end
end

function love.mousepressed(x, y, button)
    if button == 1 then
        love.mouse.setRelativeMode(true)
        if game.player.grabbed == 0 then
            local c0, s0 = math.cos(game.player.r), -math.sin(game.player.r)
            local c1, s1 = math.cos(game.player.r-math.pi*0.1), -math.sin(game.player.r-math.pi*0.1)
            local c2, s2 = math.cos(game.player.r+math.pi*0.1), -math.sin(game.player.r+math.pi*0.1)
            local len = 6
            game.world:rayCast(game.player.x, -game.player.z, game.player.x-s0*len, -game.player.z+c0*len, raycast)
            game.world:rayCast(game.player.x, -game.player.z, game.player.x-s1*len, -game.player.z+c1*len, raycast)
            game.world:rayCast(game.player.x, -game.player.z, game.player.x-s2*len, -game.player.z+c2*len, raycast)
        end
    elseif button == 2 and game.player.grabbed > 0 then
        game.physics.keys[game.player.grabbed].fixture:setMask()
        game.keys[game.player.grabbed].y = 0.25
        game.player.grabbed = 0
    end
end

function raycast(fixture, x, y, xn, yn)
    local data = fixture:getUserData()
    if data and data[1] == 'key' then
        game.player.grabbed = data[3]
        game.physics.keys[game.player.grabbed].fixture:setMask(3)
    end
    return 0
end

function love.draw()
    love.graphics.setShader(shader25d)
    shader25d:send('pmat', pmat)
    shader25d:send('vmat', vmat)
    Room.drawSub(game.room, game.player.sub, max_recurse)

    local debug = false
    if debug then
        love.graphics.setShader()
        love.graphics.setColor(255, 0, 0)
        love.graphics.scale(2, 2)
        love.graphics.translate(320, 64)
        for _, sub in ipairs(game.room) do
            if sub.gen then 
                for _, wall in ipairs(sub.walls) do
                    local x0, y0, w, r = wall[1], wall[2], wall[3], wall[4]
                    local x1, y1 = 0, 0
                    x1 = x0+w*math.cos(r*math.pi*0.5)
                    y1 = y0-w*math.sin(r*math.pi*0.5)

                    love.graphics.line(x0, y0, x1, y1)
                end
            end
        end
        love.graphics.origin()
        love.graphics.setColor(255, 255, 255)
        love.graphics.print(string.format('FPS: %i', love.timer.getFPS()), 8, 8)
        love.graphics.print(string.format('Delta: %.2f', love.timer.getAverageDelta()*1000), 8, 20)
        love.graphics.print(string.format('XYZ: %.2f %.2f %.2f', game.player.x, game.player.y, game.player.z), 8, 32)
        love.graphics.print(string.format('Subroom: %s', game.player.sub), 8, 44)
        love.graphics.print(Room.getSub(game.room, game.player.sub).description, 8, 56)
    end
end
