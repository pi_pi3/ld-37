
Room = {}

function Room.drawWall(wall, height)
    local x, y, rot = wall[1], wall[2], wall[4]
    local rad = rot*math.pi*0.5
    local mmat = Mat.rotate(rad, 'y') * Mat.translate(x, 0, y)
    shader25d:send('mmat', mmat)
    love.graphics.draw(wall.mesh)
end

function Room.drawFloor(floor, height)
    local x0, y0 = floor[1], floor[2]
    local mmat = Mat.rotate(math.pi*0.5, 'x') * Mat.translate(x0, height, y0)
    shader25d:send('mmat', mmat)
    love.graphics.draw(floor.mesh)
end

function Room.portalStencil()
    local x, y, width, height, rot = shape[1], shape[2], shape[3], shape[4], shape[5]

    local mmat = Mat.rotate(rot*math.pi*0.5, 'y') * Mat.translate(x, 0, y)
    shader25d:send('mmat', mmat)
    love.graphics.rectangle('fill', 0, 0, width, height)
end

function Room.drawPortal(room, portal, recurse)
    declare('shape', portal.shape)
    love.graphics.stencil(Room.portalStencil, 'increment', 1, recurse < max_recurse)
    shape = nil

    love.graphics.setStencilTest('greater', max_recurse-recurse)
    --love.graphics.setStencilTest()
    love.graphics.setShader()
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle('fill', 0, 0, 1000, 1000)
    love.graphics.setShader(shader25d)
    love.graphics.setColor(255, 255, 255)

    local out = {x = 0, y = 0, z = 0, r = 0}
    if portal.out and portal.out[4] then
        local rad = (portal.out[4] or 0.0)*math.pi*0.5
        out.x = (portal.out[1])
        out.z = (portal.out[3])
        out.y = portal.out[2]
        out.r = rad
    elseif portal.out then
        local rad = (portal.out[3] or 0.0)*math.pi*0.5
        out.x = (portal.out[1])
        out.z = (portal.out[2])
        out.y = 0
        out.r = rad
    end
    local c = math.cos(portal.shape[5]*math.pi*0.5)
    local s = -math.sin(portal.shape[5]*math.pi*0.5)
    local p1x, p1y, p1z = portal.shape[1]+portal.shape[3]*0.5*c, 
                          0.0, 
                          portal.shape[2]+portal.shape[3]*0.5*s
    local p2x, p2y, p2z = p1x+out.x, p1y+out.y, p1z+out.z
    local rot = Mat.translate(-p2x, -p2y, -p2z) * Mat.rotate(out.r, 'y') * Mat.translate(p2x, p2y, p2z)
    local pos = Mat.translate(-out.x, -out.y, -out.z)
    local vmat = rot * pos * _G['vmat']
    shader25d:send('vmat', vmat)
    local clip_c = math.cos(portal.shape[5]*math.pi*0.5+out.r)
    local clip_s = -math.sin(portal.shape[5]*math.pi*0.5+out.r)
    shader25d:send('clip', {p2x, p2z, clip_s, clip_c})
    Room.drawSub(room, portal.sub, recurse-1, portal.skipw, portal.skipp)

    shader25d:send('vmat', _G['vmat'])
    shader25d:send('clip', {0.0, 0.0, 0.0, 0.0})
    love.graphics.setStencilTest()
end

function Room.getSub(room, sub)
    if type(sub) == 'number' then
        return room[sub]
    elseif type(sub) == 'string' then
        for _, v in ipairs(room) do
            if v.name == sub then
                return v
            end
        end
    end
    return nil
end

function Room.drawSub(room, sub, recurse, skipw, skipp)
    recurse = recurse or 0
    skipw = skipw or 0
    skipp = skipp or 0
    local subroom = Room.getSub(room, sub)
    if not subroom then return end
    local height = subroom.height

    Room.drawFloor(subroom.floor, 0)
    Room.drawFloor(subroom.ceil, height)

    local zcomp = function(a, b)
        local da, db
        if a.shape then 
            a = a.shape
            local dx = (2*a[1]+a[3]*math.cos(a[5]*math.pi*0.5))*0.5-game.player.x
            local dy = (2*a[2]-a[3]*math.sin(a[5]*math.pi*0.5))*0.5-game.player.z
            da = math.sqrt(dx*dx+dy*dy)
        else
            local dx = (2*a[1]+a[3]*math.cos(a[4]*math.pi*0.5))*0.5-game.player.x
            local dy = (2*a[2]-a[3]*math.sin(a[4]*math.pi*0.5))*0.5-game.player.z
            da = math.sqrt(dx*dx+dy*dy)
        end
        if b.shape then 
            b = b.shape
            local dx = (2*b[1]+b[3]*math.cos(b[5]*math.pi*0.5))*0.5-game.player.x
            local dy = (2*b[2]-b[3]*math.sin(b[5]*math.pi*0.5))*0.5-game.player.z
            db = math.sqrt(dx*dx+dy*dy)
        else
            local dx = (2*b[1]+b[3]*math.cos(b[4]*math.pi*0.5))*0.5-game.player.x
            local dy = (2*b[2]-b[3]*math.sin(b[4]*math.pi*0.5))*0.5-game.player.z
            db = math.sqrt(dx*dx+dy*dy)
        end
        return da > db
    end

    local t = {}
    for i, v in ipairs(subroom.walls)   do if i ~= skipw then t[#t+1] = v end end
    for i, v in ipairs(subroom.portals) do if i ~= skipp then t[#t+1] = v end end

    local objects = t
    table.sort(objects, zcomp)

    for i, v in ipairs(objects) do
        if v.shape then
            local active = true
            if v.needs then
                for _, n in ipairs(v.needs) do
                    if not game.locks[n].active then
                        active = false
                    end
                end
            end
            if recurse > 0 and active then
                Room.drawPortal(room, v, recurse)
            else
                local shape = v.shape
                local x, y, width, height, rot = shape[1], shape[2], shape[3], shape[4], shape[5]

                local mmat = Mat.rotate(rot*math.pi*0.5, 'y') * Mat.translate(x, 0, y)
                shader25d:send('mmat', mmat)
                love.graphics.setColor(0, 0, 0)
                love.graphics.rectangle('fill', 0, 0, width, height)
                love.graphics.setColor(255, 255, 255)
            end
        else
            Room.drawWall(v, height) 
        end
    end

    love.graphics.setColor(200, 40, 0)
    for i, v in ipairs(game.locks) do
        if subroom.name == v.sub then
            GFX.drawCube(v.x, v.y, v.z, 2*v.w, 0.1, 2*v.w)
        end
    end

    love.graphics.setColor(0, 96, 192)
    for i, v in ipairs(game.keys) do
        if subroom.name == v.sub then
            GFX.drawCube(v.x, v.y, v.z, 2*v.w)

        end
    end

    love.graphics.setColor(255, 255, 255)
end

return Room
