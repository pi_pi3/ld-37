
Mat = {}
Mat.id = {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}

function Mat.new(t)
    local m = {t[1],  t[2],  t[3],  t[4],
               t[5],  t[6],  t[7],  t[8],
               t[9],  t[10], t[11], t[12],
               t[13], t[14], t[15], t[16]}
    setmetatable(m, Mat.mt)
    return m
end

function Mat.identity()
    return Mat.new(Mat.id)
end

function Mat.perspective(fov, n, f, ratio)
    local r = 0.001 * ratio
    local t = 0.001
    return Mat.new({-n/r, 0, 0, 0,
                    0, -n/t, 0, 0,
                    0, 0, -(f+n)/(f-n), -1,
                    0, 0, (f*n)/(f-n), 0})
end

function Mat.orthographic(n, f, r, t)
    return Mat.new({1/r, 0, 0, 0,
                    0, 1/t, 0, 0,
                    0, 0, 2/(f-n), 0,
                    0, 0, -(f+n)/(f-n), 1})
end

function Mat.translate(x, y, z)
    return Mat.new({1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    x, y, z, 1})
end

function Mat.rotate(r, axis)
    local c = math.cos(r)
    local s = math.sin(r)
    if axis == 'x' then
        return Mat.new({1,  0,  0, 0,
                        0,  c,  s, 0,
                        0, -s, c, 0,
                        0,  0,  0, 1})
    elseif axis == 'y' then
        return Mat.new({c, 0, -s, 0,
                        0, 1,  0, 0,
                        s, 0,  c, 0,
                        0, 0,  0, 1})
    elseif axis == 'z' then
        return Mat.new({c, s, 0, 0,
                       -s, c, 0, 0,
                        0, 0, 1, 0,
                        0, 0, 0, 1})
    else return nil end
end

function Mat.scale(x, y, z)
    return Mat.new({x, 0, 0, 0,
                    0, y, 0, 0,
                    0, 0, z, 0,
                    0, 0, 0, 1})
end

function Mat.add(a, b)
    return Mat.new({a[1]+b[1], a[2]+b[2], a[3]+b[3], a[4]+b[4],
                    a[5]+b[5], a[6]+b[6], a[7]+b[7], a[8]+b[8],
                    a[9]+b[9], a[10]+b[10], a[11]+b[11], a[12]+b[12],
                    a[13]+b[13], a[14]+b[14], a[15]+b[15], a[16]+b[16]})
end

function Mat.sub(a, b)
    return Mat.new({a[1]-b[1], a[2]-b[2], a[3]-b[3], a[4]-b[4],
                    a[5]-b[5], a[6]-b[6], a[7]-b[7], a[8]-b[8],
                    a[9]-b[9], a[10]-b[10], a[11]-b[11], a[12]-b[12],
                    a[13]-b[13], a[14]-b[14], a[15]-b[15], a[16]-b[16]})
end

function Mat.mult(a, b)
    return Mat.new({
        -- row 0
        a[1]*b[1]  + a[2]*b[5]  + a[3]*b[9]   + a[4]*b[13],  a[1]*b[2]  + a[2]*b[6]  + a[3]*b[10]  + a[4]*b[14],
        a[1]*b[3]  + a[2]*b[7]  + a[3]*b[11]  + a[4]*b[15],  a[1]*b[4]  + a[2]*b[8]  + a[3]*b[12]  + a[4]*b[16],
        -- row 1
        a[5]*b[1]  + a[6]*b[5]  + a[7]*b[9]   + a[8]*b[13],  a[5]*b[2]  + a[6]*b[6]  + a[7]*b[10]  + a[8]*b[14],
        a[5]*b[3]  + a[6]*b[7]  + a[7]*b[11]  + a[8]*b[15],  a[5]*b[4]  + a[6]*b[8]  + a[7]*b[12]  + a[8]*b[16],
        -- row 2
        a[9]*b[1]  + a[10]*b[5] + a[11]*b[9]  + a[12]*b[13], a[9]*b[2]  + a[10]*b[6] + a[11]*b[10] + a[12]*b[14],
        a[9]*b[3]  + a[10]*b[7] + a[11]*b[11] + a[12]*b[15], a[9]*b[4]  + a[10]*b[8] + a[11]*b[12] + a[12]*b[16],
        -- row 3
        a[13]*b[1] + a[14]*b[5] + a[15]*b[9]  + a[16]*b[13], a[13]*b[2] + a[14]*b[6] + a[15]*b[10] + a[16]*b[14],
        a[13]*b[3] + a[14]*b[7] + a[15]*b[11] + a[16]*b[15], a[13]*b[4] + a[14]*b[8] + a[15]*b[12] + a[16]*b[16]
    })
end

Mat.mt = {}
Mat.mt.__index = Mat.id
Mat.mt.__add = Mat.add
Mat.mt.__sub = Mat.sub
Mat.mt.__mul = Mat.mult

return Mat

